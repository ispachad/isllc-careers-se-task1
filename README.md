# Insurance Services - Software Engineer Applicant Task 1
Hello!  Thank you for your interest in our company.  The first part of our application process asks you to develop a small API service. Please read below for further instructions and good luck!

## BACKGROUND
We are an insurance brokerage.  This allows us to offer products from multiple carriers to best meet client needs.  
In this task, we're building a Rates API to organize all of the different rates for a particular product called a Medicare Supplement (a.k.a. Medigap policy).  


## TASK 1 - Develop a Medicare Supplement Rates API.

Data directory contains:

* **Rates.csv** - sample medicare supplement rate data
* **zip_codes.csv** - zip code lookup data
* **Field Descriptions NO UW.xls** - field descriptions for data fields in above files


### REQUIREMENTS
Your api needs only a single endpoint which provides a response to a `GET` request.

Here are more details:

#### Query Parameters

Field | Type | Details | Required?
----- | ---- | ------- | ---------
zip_code | String | A 5-digit US Postal Zip Code | required |
age | String | Applicant's age or an age symbol (e.g. "65D" for < 65 Medicare Disability) | required |
gender | String | M or F | optional |
tobacco | String | Y or N | optional |

#### Response body Parameters
We are not requiring a "strictly defined" response aside from directing you to implement only JSON response format and include all available fields.


#### Sample Request

This is an example GET request we will make against your api to test your results:

    curl --request GET \ 
	-H 'Accept: application/json' \
    --url 'http://exampleapi.xyz/medigap?age=70&zip_code=37024' 
   
Here is another:

    curl --request GET \ 
	-H 'Accept: application/json' \
    --url 'http://exampleapi.xyz/medigap?age=70&zip_code=37024&tobacco=Y&gender=M' 


### COMPLETING TASK 1
When you are ready to submit your code, please deploy a working version.

Then send an email to careers@ispahq.com **using the subject line 'Software Engineer Task 1'** and include:

1. A link to your public repo
2. A link to your working API (or put the link in your README)
3. Any notes or details you feel relevant to your submission


### FAQS for Guidance
Q. How long do I have to complete the task?
A. We know you're busy, so there is no set time limit for Task 1.  But we will close the job posting at some point (probably by early March 2018).  So don't wait too long!

Q. What language/framework/stack should I use?
A. Your choice, just be prepared to defend your decision.

Q. Do I need to worry about security?
A. Task 1 is not meant to be representative of a production-worthy system, so there's no need to spend time on hardening your API for security concerns.  This includes rate-limiting, api tokens, etc.

Q. Do I need to use a database?
A. Ideally you'll have some data storage tier in your stack that offers fast lookups and can scale to hold tens-of-thousands of rows of rate data down the road.

Q. Do I need to worry about caching?
A. Not for Task 1



